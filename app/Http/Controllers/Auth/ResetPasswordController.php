<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\User;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'update']);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'oldpwd' => 'required|string|min:8',
                'newpwd' => 'required|string|min:8',
                'comnewpwd' => 'required|string|min:8|same:newpwd'
        ]);

        if ($validator->fails()) {
            alert()->error('Change Password Fail')->position('top-end')->autoClose(1500);
            return redirect('home');
        }


        $data = $request->all();
        $user = User::find(auth()->user()->id);

        if (Hash::check($data['oldpwd'], $user->password)) {
            $user = new User;
            $user->password = Hash::make($request->newpwd);
            $user->update();
            alert()->success('Change Password Successfully')->position('top-end')->autoClose(1500);
            return redirect('home');
        } else {
            alert()->error('Change Password Fail','Your old passowrd is wrong')->position('top-end')->autoClose(1500);
            return redirect('home');
        }
    }
}
