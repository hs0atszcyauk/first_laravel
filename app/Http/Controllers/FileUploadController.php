<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FileUploadController extends Controller
{
    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048|required'
        ]);

        if ($validator->fails()) {
            alert()->error('Upload Image Fail')->position('top-end')->autoClose(2000);
            return redirect('home');
        }

            
        $fileNameWithExt = $request->file('image')->getClientOriginalName();
        $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('image')->getClientOriginalExtension();
        $fileNameToStore = $fileName.'_'.time().'_'.$extension;

        $path = $request->file('image')->storeAs('public/image', $fileNameToStore);

        User::where('id', Auth::user()->id)->update([
                'icon_location' => 'storage/image/'.$fileNameToStore
                ]);
        alert()->success('Upload Image Successfully')->position('top-end')->autoClose(1500);
        return redirect('home');
    }
}
