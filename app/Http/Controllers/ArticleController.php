<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use App\Article;
use Illuminate\Support\Facades\Auth;
use Purifier;
use RealRashid\SweetAlert\Facades\Alert;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    public function index()
    {
        $query = Article::orderBy('created_at', 'desc')->get();
        return view('article.index', compact('query', 'test'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $secret = '6LfNH6EUAAAAAEtMHv8Xi5FvmRBq-o-t7B5WbdB8';
        $sitekey = '6LfNH6EUAAAAAG28W2x9Tjs_T_arstC6_hWkhXHC';
        $captcha = new \Anhskohbo\NoCaptcha\NoCaptcha($secret, $sitekey);
        //return $request->all();
        if (strlen($request->title) > 255) {
            session()->flash('alertType', 'Fail!');
            session()->flash('type', 'alert-danger');
            session()->flash('message', 'The total name should be less than 255 words!');
            return redirect('article/create');
        } else {
            if ($captcha->verifyResponse($_POST['g-recaptcha-response'])) {
                // Article::create([
                // 'title' => $request->get('title'),
                // 'content' => $request->get('content'),
                // 'user_id' => Auth::id() 
                // ]);
                $article = new Article;
                $article->title = $request->title;
                $article->content = $request->content;
                $article->user_id = Auth::id();
                $article->save();
                
                // session()->flash('alertType','Success!');
                // session()->flash('type','alert-success');
                // session()->flash('message','Create Successfully');
                alert()->success('Create Article Successfully')->position('top-end')->autoClose(1500);
                return redirect('article');
            }else{
                return back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('article');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query = Article::find($id);
        if ($query->user_id == Auth::id()) {
            return view('article.edit', compact('query'));
        } else {
            return "Don't Hack Me OK?";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $secret = '6LfNH6EUAAAAAEtMHv8Xi5FvmRBq-o-t7B5WbdB8';
        $sitekey = '6LfNH6EUAAAAAG28W2x9Tjs_T_arstC6_hWkhXHC';
        $captcha = new \Anhskohbo\NoCaptcha\NoCaptcha($secret, $sitekey);

        $query = Article::find($id);
        if ($query->user_id == Auth::id()) {
            if ($captcha->verifyResponse($_POST['g-recaptcha-response'])) {
                $this->validate($request, [
                'content' => 'required|min:10',
                'g-recaptcha-response' => 'required'
                ]);

                Article::where('id', $id)->update([
                'content' => $request->get('content')
                ]);
                // session()->flash('alertType','Success!');
                // session()->flash('type','alert-success');
                // session()->flash('message','Update Successfully');
                alert()->success('Update Article Successfully')->position('top-end')->autoClose(1500);
                return redirect('article');
            }else{
                return back();

            }
            
        } else {
            return "Don't Hack Me OK?";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Article::find($id);
        if ($query->user_id == Auth::id()) {
            $query->delete();
            // session()->flash('alertType','Success!');
            // session()->flash('type','alert-success');
            // session()->flash('message','Delete Successfully');
            //alert()->success('Delete Article Successfully')->position('top-end')->autoClose(1500);
            return redirect('article');
        } else {
            return "Don't Hack Me OK?";
        }
    }

    public function search(Request $request)
    {
        // if ($request->ajax()) {
        //     $query = $request->get('query');
        //     if($query != ''){
        //         $data = DB::table('articles')->where('title','like','%'.$query.'%')->orwhere('author','like','%'.$query.'%')->orderBy('created_at','desc')->get();
        //     }
        //     else{
        //         $data = DB::table('articles')->orderBy('created_at','desc')->get();
        //     }
        // }
        // $total_row = $data->count();
        if ($request->ajax()) {
            echo Article::where('id', $request->get('id'))->get('content');
        }
    }
}
