<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use Mail;

class MailController extends Controller
{
    public function send(request $request){
        // Mail::send(['text'=>'mail'], ['name'=>'Ken So'], function ($message) {
        //     $message->from('kswebapplication@gmail.com', 'Laravel KS Web');
        //     $message->to('kenso34911047@gmail.com');
        //     $message->subject('KS Web Reset Password');
        // });
        Mail::send(new sendMail());
        session()->flash('type','alert-success');
        session()->flash('message','Send Email Successfully');
        return redirect('home');
        //return $request->all();
    }
}
