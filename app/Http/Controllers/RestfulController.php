<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;
use App\Http\Requests;
use App\Book;
use App\Http\Resources\Book as BookResource;

class RestfulController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Book::paginate(5);
        return BookResource::collection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$query = $request->isMethod(put) ? Book::findOrFail($request->)
        try{

            $query = Book::create($request->all());
            return new BookResource($query);

        }catch(\Exception $e){

            return "Something Error";

        }
        // $query = Book::create([
        //     'title' => $request->get('title'),
        //     'content' => $request->get('content'),
        //     'remark' => $request->get('remark')
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = Book::findOrFail($id);
        return new BookResource($query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $query = Book::findOrFail($id);

            Book::where('id',$id)->update([
                'title' => $request->get('title'),
                'content' => $request->get('content'),
                'remark' => $request->get('remark')
            ]);

            return new BookResource($query);

        }catch(\Exception $e){
            return "Something Error";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $query = Book::findOrFail($id);
            if($query->delete()){
                return new BookResource($query);
            }
            else{
                return "Delete Fail";
            }

        }catch(\Exception $e){
            return "Something Error";
        }
    }
}
