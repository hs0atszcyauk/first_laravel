<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles'; 
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'content','author'];

    public function getTitleAttribute($value){
        return ucfirst($value);
    }
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
