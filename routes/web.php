<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('article', 'ArticleController');

Route::get('search','ArticleController@search')->name('article.search');

Route::resource('api', 'RestfulController');

Route::post('/upload', 'FileUploadController@upload')->name('upload.add');
Route::post('/updatepassword', 'Auth\ResetPasswordController@update')->name('updatepassword');


Route::get('/home', 'HomeController@index')->name('home');

Route::get('send', 'MailController@send')->name('send');

Auth::routes();

Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');





