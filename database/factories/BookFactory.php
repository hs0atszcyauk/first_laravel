<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'title' => $faker->text(15),
        'content' => $faker->text(200),
        'remark' => $faker->unique()->safeEmail
    ];
});
