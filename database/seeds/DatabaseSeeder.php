<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Article;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('articles')->truncate();

        // for ($i=0; $i < 10; $i++){ 
        //     Article::create([
        //         'title' => str_random(10),
        //         'content'=> str_random(255),
        //         'author' => 'root'
        //     ]);
        // }
        $this->call(BooksTableSeeder::class);
    
    }
}
