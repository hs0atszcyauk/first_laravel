@if (session()->has('message'))
<div class="alert {{session()->get('type')}} alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{{session()->get('alertType')}}</strong> {{session()->get('message')}}
</div>
@endif
