@include('layouts.message')

<section class="container">
    {!! Form::open(['action'=>$controllerMethod,'method'=>$httpMethod])!!}

    <div class="form-group">
        {{Form::label('title','Title')}}
        {{Form::text('title',$titleData,['class'=>'form-control','placeholder'=>'Title','required'=>'true',$disabled])}}
    </div>

    <div class="form-group">
        {{Form::label('content','Content')}}
        {{Form::textarea('content',$contentData,['class'=>'form-control','placeholder'=>'Body Text','minlength'=>'10','required'=>'true'])}}
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            @php
            $secret = '6LfNH6EUAAAAAEtMHv8Xi5FvmRBq-o-t7B5WbdB8';
            $sitekey = '6LfNH6EUAAAAAG28W2x9Tjs_T_arstC6_hWkhXHC';
            $captcha = new \Anhskohbo\NoCaptcha\NoCaptcha($secret, $sitekey);
            echo $captcha->display();
            @endphp
        </div>
    </div>

    {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
    {{Form::close()}}

    <br>

    @if (count($errors)>0)
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$error}}
    </div>
    @endforeach
    @endif
</section>

{{-- <form action="{{ url('article')}} " method="post">
<input type="hidden" name="_token" value="{{csrf_token()}}" required>
Title: <input type="text" name="title" class="form-control" maxlength="255" required>
<br>
Content: <textarea name="content" cols="30" rows="10" minlength="10" class="form-control" required></textarea>
<br>
<input type="submit" value="Submit" class="btn btn-primary">
</form> --}}

@section('js')
{!! NoCaptcha::renderJs() !!}
@stop