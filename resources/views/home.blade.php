@extends('layouts.app')

@section('content')
@include('layouts.message')
<div class="container">
    <div class="row justify-content-center">
        <div class="card" style="width: 18rem;">
            <img src="{{Auth::user()->icon_location}}" class="card-img-top">
            <div class="card-body">
                <p class="card-text">Welcome to Here, you can choose your own photo and upload it to become your icon.
                </p>
                <form action="{{ route('upload.add')}}" method="post" enctype="multipart/form-data">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile" name="image">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    <input type="hidden" name="_token" value="{{csrf_token()}}" required>
                    <input type="submit" value="Upload Image" class="btn btn-primary" style="margin-top:20px">
                </form>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body" style="height:500px">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <h4>You are logged in ! Now You can create new article or send email.</h4>
                    <br><br>
                    <form class="form-group" method="POST" action="{{route('updatepassword')}}">
                        {{csrf_field()}}
                        <h4>CHANGE PASSWORD</h4>
                        <br>
                        <div class="form-group">
                            <label for="oldpwd">Old Password</label>
                            <input type="password" class="form-control" id="oldpwd" minlength="8" name="oldpwd"
                                aria-describedby="emailHelp" placeholder="Enter old password" required>
                        </div>
                        <div class="form-group">
                            <label for="newpwd">New Password</label>
                            <input type="password" class="form-control" id="newpwd" minlength="8" name="newpwd"
                                placeholder="Enter new password" required>
                        </div>
                        <div class="form-group">
                            <label for="comnewpwd">Comfirm New Password</label>
                            <input type="password" class="form-control" id="comnewpwd" minlength="8" name="comnewpwd"
                                placeholder="Enter new password again" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

            </div>
        </div>


    </div>
    <br>
    <div class="container">
        <h2>Send Email by System</h2>
        <br>
        <form class="form-horizontal form-group" method="POST" action="{{route('send')}}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="GET">

            <label class="control-label col-sm-2" for="email">To:</label>
            <input type="email" class="form-control col-sm-10" name="email" placeholder="Enter email" required>
            <br>
            <label class="control-label col-sm-2" for="message">Content</label>
            <textarea placeholder="write your email message here" rows="10" class="form-control col-sm-10"
                minlength="10" name="message" required></textarea>
            <br>

            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
    </div>
</div>
@endsection