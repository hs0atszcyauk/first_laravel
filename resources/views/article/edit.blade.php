@extends('layouts.app')

@section('content')
@php
$disabled = 'disabled';
$titleData = $query->title;
$contentData = $query->content;
$controllerMethod = ['ArticleController@update',$query->id];
$httpMethod = 'PUT';
@endphp
@include('layouts.form')
@stop