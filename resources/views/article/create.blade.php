@extends('layouts.app')

@section('content')
@php
$disabled = '';
$titleData = '';
$contentData = '';
$controllerMethod = 'ArticleController@store';
$httpMethod = 'POST';
@endphp
@include('layouts.form')
@stop