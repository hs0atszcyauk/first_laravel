@extends('layouts.app')

@section('cssStyle')
<style>
    td {
        word-wrap: break-word
    }

    table {
        table-layout: fixed
    }
</style>
@stop


@section('content')
@include('layouts.message')

<section class="container">
    @guest
    <h4>You need to <a href="{{route('login')}}">login</a> if you want to create an article</h4>
    @else
    <a href="{{url('article/create')}}" role="btn" class="btn btn-primary">Create New Article</a>
    @endguest
    <br><br>
    <table class="table table-hover">
        <thead>
            {{-- <th>ID</th> --}}
            <th colspan="2">Title</th>
            <th>Author</th>
            <th>Time</th>
            <th></th>
            <th></th>
            <th></th>
        </thead>
        @foreach($query as $var)
        <tr>
            {{-- <td style="padding:.9rem">{{$var->id}}</td> --}}
            <td style="padding:.9rem" colspan="2">{{$var->title}}</td>
            <td>{{App\User::find($var->user_id)->name}}</td>
            <td>{{$var->created_at->diffForHumans()}}</td>
            <td><button type="button" class="btn btn-secondary" data-toggle="collapse"
                    onclick="showArticle({{$var->id}})" data-target="{{'#articleData'.$var->id}}">Show</button>
            </td>
            @guest
            <td></td>
            <td></td>
            @else
            @if ($var->user_id == Auth::user()->id)
            <td>
                <a href="{{url('article/'.$var->id.'/edit')}}" role="btn" class="btn btn-success">Edit</a>
            </td>
            <td>
                <a href="javascript:void(0)" role="btn" class="btn btn-danger delete"
                    onclick="deleteArticle({{$var->id}})">Delete</a>
            </td>
            @else
            <td></td>
            <td></td>
            @endif
            @endguest
        </tr>
        <tr id="{{'content'.$var->id}}" hidden="true">
            <td colspan="7">
                <div id="{{'articleData'.$var->id}}" class="collapse">
                    {!!Purifier::clean(nl2br($var->content))!!}
                </div>
            </td>
        </tr>
        @endforeach
    </table>
</section>
@stop


@section('js')
<script>
    function deleteArticle(value) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "http://54.199.226.19/article/" + value + "?_method=delete&_token=" + {!!'"'.csrf_token().'"'!!},
                    "method": "POST"
                }
                $.ajax(settings).done(function (response) {
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'Delete Article Successfully',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    setTimeout(() => {
                        location.reload();
                    }, 1500);
                });
            }
        })
    }

    function showArticle(value) {
        if ($('#content' + value)[0].getAttribute('hidden')) {

            $('#content' + value)[0].removeAttribute('hidden');
            $(event.target).text('Hide')

        } else {

            $('#content' + value)[0].setAttribute('hidden', 'true');
            $(event.target).text('Show')
        }
    }
</script>
@stop